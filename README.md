﻿# 豆瓣api使用  

BaseUrl = "https://douban.uieee.com/v2"  
BaseUrl = "https://api.douban.com/v2"  
获取电影信息  

## 电影条目搜索  

<https://api.douban.com/v2/movie/search>  
Example:  

* GET <https://api.douban.com/v2/movie/search?q=张艺谋>  
* GET <https://api.douban.com/v2/movie/search?tag=喜剧>  
  
## 正在热映  

<https://api.douban.com/v2/movie/in_theaters>  
  
Example:  
GET <https://api.douban.com/v2/movie/in_theaters>

* top250  
<https://api.douban.com/v2/movie/top250>  
  
Example:  
GET <https://api.douban.com/v2/movie/top250>  

## 口碑榜  

<https://api.douban.com/v2/movie/weekly>  
  
Example:  
GET <https://api.douban.com/v2/movie/weekly>

## 北美票房榜  

<https://api.douban.com/v2/movie/us_box>  
  
Example:  
GET <https://api.douban.com/v2/movie/us_box>

## 新片榜  

<https://api.douban.com/v2/movie/new_movies?apikey=xxx>  
  
Example:  
GET <https://api.douban.com/v2/movie/new_movies?apikey=0df993c66c0c636e29ecbb5344252a4a>

## 获取电影信息  

<https://api.douban.com/v2/movie/:id>  
  
Example:  
GET <https://api.douban.com/v2/movie/4864908>

## 获取电影剧照  

<https://api.douban.com/v2/movie/subject/:id/photos?count=xxx&apikey=xxxx>  
  
Example:  
GET <https://api.douban.com/v2/movie/subject/26825664/photos?count=100&apikey=0df993c66c0c636e29ecbb5344252a4a>

## 获取电影条目短评论  

<https://api.douban.com/v2/movie/subject/:id/comments?start=xxx&count=xxx&apikey=xxxx>  
  
Example:  
GET <https://api.douban.com/v2/movie/subject/26825664/comments?start=1&count=100&apikey=0df993c66c0c636e29ecbb5344252a4a>

## 获取电影条目长评论  

<https://api.douban.com/v2/movie/subject/:id/reviews?start=xxx&count=xxx&apikey=xxxx>  
  
Example:  
GET <https://api.douban.com/v2/movie/subject/26004132/reviews?apikey=0b2bdeda43b5688921839c8ecb20399b>  

## 获取影人条目信息  

<https://api.douban.com/v2/movie/celebrity/:id?start=xxx&count=xxx&apikey=xxxx>  
  
Example:  
GET <https://api.douban.com/v2/movie/celebrity/1044707?apikey=0b2bdeda43b5688921839c8ecb20399b>  
  
## 获取影人剧照  

<https://api.douban.com/v2/movie/celebrity/:id/photos?start=xxx&count=xxx&apikey=xxxx>  
  
Example:  
GET <https://api.douban.com/v2/movie/celebrity/1044707/photos?apikey=0b2bdeda43b5688921839c8ecb20399b>>  
  
GET <https://api.douban.com/v2/movie/celebrity/1044707/photos?start=1&count=100&apikey=0b2bdeda43b5688921839c8ecb20399b>>  
  
## 获取影人作品  

<https://api.douban.com/v2/movie/celebrity/:id/works?start=xxx&count=xxx&apikey=xxxx>  
  
Example:  
GET <https://api.douban.com/v2/movie/celebrity/1044707/works?apikey=0b2bdeda43b5688921839c8ecb20399b>  
  
GET <https://api.douban.com/v2/movie/celebrity/1044707/works?start=1&count=100&apikey=0b2bdeda43b5688921839c8ecb20399b>  

## 搜索音乐

<https://api.douban.com/v2/music/search>  
  
Example:  
GET <https://api.douban.com/v2/music/search?q=zsh>  

## 获取音乐信息  

<https://api.douban.com/v2/music/:id>  
  
Example:  
GET <https://api.douban.com/v2/music/2337701>  
  
## 搜索图书  

<https://api.douban.com/v2/book/search>  
  
Example:  
GET <https://api.douban.com/v2/book/search?q=zsh>

## 获取图书信息  

<https://api.douban.com/v2/book/:id>  
  
Example:  
GET <https://api.douban.com/v2/book/25902185>  

## 【 Android 】豆瓣电影 API 指南  

豆瓣电影是人人皆知的一个版块，他的评分很具有代表性。我们就选用豆瓣电影 API 来进行电影类的数据获取。本篇文章应该是全网最新最全的 API 指南。  

### 一、豆瓣电影 API 官方文档  

API接口文档分成4大部分，14小部分：  
  
#### 电影条目  
  
电影条目信息  
电影条目剧照  
电影条目长评  
电影条目短评

#### 影人条目  
  
影人条目信息  
影人剧照  
影人作品  
搜索  
  
#### 电影搜索

榜单  
正在热映  
即将上映  
Top250  

#### 口碑榜1

北美票房榜  
新片榜  
